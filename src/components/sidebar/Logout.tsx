import LogoutLogo from "../../assets/logout.svg";
import styles from "./Sidebar.module.css";

const Logout = () => {
    return (
        <div className={styles.logout}>
            <img src={LogoutLogo} alt="Logout Icon" />
            <h3>Logout</h3>
        </div>
    );
};

export default Logout;
