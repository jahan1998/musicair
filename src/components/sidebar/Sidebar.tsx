import { BrowserRouter as Router, Switch, Route, NavLink } from "react-router-dom";
import Logout from "./Logout";

import Logo from "../../assets/music-air-logo.png";
import styles from "./Sidebar.module.css";

interface Item {
    name: string;
    to: string;
    childComponent: React.ReactNode;
    svg?: React.ReactNode;
    color?: string;
}

interface IProps {
    userName: string;
    items: Item[];
}

const Sidebar: React.FC<IProps> = ({ userName, items }) => {
    // Generates and returns a list of NavLinks which are displayed in the sidebar.
    const genListItems = (): JSX.Element[] => {
        return items.map((item) => {
            return (
                <li>
                    <NavLink
                        to={item.to}
                        style={{ color: item.color, borderColor: item.color }}
                        activeClassName={styles.active}
                        className={styles.navbtn}
                    >
                        {item.svg}
                        <h3>{item.name}</h3>
                    </NavLink>
                </li>
            );
        });
    };

    // Generates the routes used to connect to the pages.
    const genRoutes = (): JSX.Element[] => {
        return items.map((item) => {
            return (
                <Route path={item.to} exact>
                    {item.childComponent}
                </Route>
            );
        });
    };

    return (
        <Router>
            <div className={styles.sidebar}>
                <nav>
                    <img src={Logo} alt="Music Air Logo" className={styles.logo} />
                    <h2 className={styles.username}>{userName}</h2>
                    <ul>
                        {genListItems()}
                        <li>
                            <Logout />
                        </li>
                    </ul>
                </nav>
            </div>

            <div className={styles.main}>
                <Switch>{genRoutes()}</Switch>
            </div>
        </Router>
    );
};

export default Sidebar;
