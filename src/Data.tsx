/**
 * Data file contains information about the pages used in the app.
 * For example the sidebarInfo has been created here which defines the information in the sidebar and the connection to the component.
 */

import Dashboard from "./pages/distributor/Dashboard";
import Royalties from "./pages/distributor/Royalties";
import Tracks from "./pages/distributor/Tracks";

// SVG images
import house from "./assets/house.svg";
import musicNote from "./assets/musical-double-note.svg";
import money from "./assets/money.svg";

export type userClass = "Distributor" | "Listener" | "Advertiser" | "Product Owner";

export const distributorSidebarInfo = [
    {
        name: "Dashboard",
        to: "/dashboard",
        childComponent: <Dashboard />,
        svg: <img src={house} alt="house icon" />,
        color: "blue",
    },
    {
        name: "Tracks",
        to: "/tracks",
        childComponent: <Tracks />,
        svg: <img src={musicNote} alt="musical note icon" />,
        color: "#00FF00",
    },
    {
        name: "Royalties",
        to: "/royalties",
        childComponent: <Royalties />,
        svg: <img src={money} alt="money icon" />,
        color: "#ff6600",
    },
];
