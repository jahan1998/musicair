import "./App.css";
import Sidebar from "./components/sidebar/Sidebar";
import { distributorSidebarInfo } from "./Data";

function App() {
    return (
        <div className="App">
            <Sidebar userName="Distributor Name" items={distributorSidebarInfo} />
        </div>
    );
}

export default App;
